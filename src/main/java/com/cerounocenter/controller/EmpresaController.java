package com.cerounocenter.controller;

import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cerounocenter.model.EmpresaDTO;
import com.cerounocenter.model.MessageResponse;
import com.cerounocenter.service.EmpresaService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/empresa")
public class EmpresaController extends ParentController {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	@Qualifier("empresaService")
	private EmpresaService empresaService;

	@PostMapping("/register")
	public ResponseEntity<?> registrar(@Validated @RequestBody EmpresaDTO empresaModel, HttpServletRequest request) {
		String ip = getClientIpAddr(request).getHostAddress();
		String empresa;
		try {
			empresa = empresaService.existsEmailEmpresa(empresaModel.getEmail());
		} catch (Exception e) {
			MessageResponse messageResponse = new MessageResponse(e.getMessage());
			super.registraLog(Level.SEVERE, messageResponse.toString(), "REQUEST", "register", EmpresaController.class.getCanonicalName(),ip);
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
		if( empresa != null){
			MessageResponse messageResponse = new MessageResponse("El email ya se encuentra registrado.");
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
		try {
			empresa = empresaService.existsIdentificacionEmpresa(empresaModel.getIdentificacion());
		} catch (Exception e) {
			MessageResponse messageResponse = new MessageResponse(e.getMessage());
			super.registraLog(Level.SEVERE, messageResponse.toString(), "REQUEST", "register", EmpresaController.class.getCanonicalName(),ip);
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
		if( empresa != null){
			MessageResponse messageResponse = new MessageResponse("El email ya se encuentra registrado.");
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
		try {			
			String resp = empresaService.save(empresaModel);
			MessageResponse messageResponse = new MessageResponse(resp);
			super.registraLog(Level.INFO, messageResponse.toString(), "RESPONSE", "register", EmpresaController.class.getCanonicalName(),ip);
			return ResponseEntity.ok(messageResponse);
		} catch (Exception e) {
			MessageResponse messageResponse = new MessageResponse(e.getMessage());
			super.registraLog(Level.SEVERE, messageResponse.toString(), "REQUEST", "register", EmpresaController.class.getCanonicalName(),ip);
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
	}
	
}
