package com.cerounocenter.controller;

import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cerounocenter.config.JwtTokenUtil;
import com.cerounocenter.model.MessageResponse;
import com.cerounocenter.model.UserDTO;
import com.cerounocenter.model.UserRequest;
import com.cerounocenter.model.UserResponse;
import com.cerounocenter.service.JwtUserDetailsService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/acceso")
public class AccesoController extends ParentController {

	private static final long serialVersionUID = 294387713679618593L;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenUtil jwtUtils;
	@Autowired
	private JwtUserDetailsService userDetailsService;

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody UserRequest loginRequest, HttpServletRequest request) {
		UserDTO userDetails;
		Authentication authentication = null;
		String ip = getClientIpAddr(request).getHostAddress();
		super.registraLog(Level.INFO, loginRequest.toString(), "REQUEST", "login", AccesoController.class.getCanonicalName(),ip);
		try {
			authentication = getAuthenticate(authenticationManager, loginRequest.getUsername(), loginRequest.getPassword());
		} catch (Exception e) {			
			MessageResponse messageResponse = new MessageResponse(e.getMessage());
			super.registraLog(Level.SEVERE, messageResponse.toString(), "RESPONSE", "login", AccesoController.class.getCanonicalName(),ip);
			return new ResponseEntity<>(messageResponse, HttpStatus.NOT_ACCEPTABLE);
		}
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generarTokenPrincipal(authentication);
		userDetails = (UserDTO) authentication.getPrincipal();
		UserResponse response = new UserResponse();
		response.setToken(jwt);
		response.setUsername(userDetails.getUsername());
		response.setId(userDetails.getId());
		super.registraLog(Level.INFO, response.toString(), "RESPONSE", "login", AccesoController.class.getCanonicalName(),ip);
		return ResponseEntity.ok(response);
	}

	@PostMapping("/register")
	public ResponseEntity<?> registrar(@Validated @RequestBody UserRequest userRequest, HttpServletRequest request) {
		String ip = getClientIpAddr(request).getHostAddress();
		if (userRequest.getUsername() == null) {
			MessageResponse messageResponse = new MessageResponse("Usuario no puede ser null.");
			super.registraLog(Level.SEVERE, messageResponse.toString(), "REQUEST", "register", AccesoController.class.getCanonicalName(),ip);
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
		if (userDetailsService.existsByUsername(userRequest.getUsername())) {
			MessageResponse messageResponse = new MessageResponse("Usuario ya esta registrado.");
			super.registraLog(Level.SEVERE, messageResponse.toString(), "REQUEST", "register", AccesoController.class.getCanonicalName(),ip);
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
		try {			
			String resp = userDetailsService.save(userRequest);
			MessageResponse messageResponse = new MessageResponse(resp);
			super.registraLog(Level.INFO, messageResponse.toString(), "RESPONSE", "register", AccesoController.class.getCanonicalName(),ip);
			return ResponseEntity.ok(messageResponse);
		} catch (Exception e) {
			MessageResponse messageResponse = new MessageResponse(e.getMessage());
			super.registraLog(Level.SEVERE, messageResponse.toString(), "REQUEST", "register", AccesoController.class.getCanonicalName(),ip);
			return new ResponseEntity<>(messageResponse, HttpStatus.BAD_REQUEST);
		}
	}	

}
