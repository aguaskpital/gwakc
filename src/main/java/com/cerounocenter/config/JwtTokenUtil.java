package com.cerounocenter.config;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;


import com.cerounocenter.model.UserDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final long JWT_TOKEN_VALIDITY = 24 * 60 * 60;

	@Value("${jwt.secret}")
	private String secret;

	public String getUserNameFromToken(String token) {
		return getRecursoDelToken(token, Claims::getSubject);
	}

	public Date getExpirationDateFromtoken(String token) {
		return getRecursoDelToken(token, Claims::getExpiration);
	}

	public <T> T getRecursoDelToken(String token, Function<Claims, T> claimsResolver) {
		final Claims recursos = getAllRecursosDelToken(token);
		return claimsResolver.apply(recursos);
	}
	
	private Claims getAllRecursosDelToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	private Boolean isTokenExpired(String token) {        
		final Date expiration = getExpirationDateFromtoken(token);
		return expiration.before(new Date());
	}

	public String generarToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		return crearToken(claims, userDetails.getUsername());
	}

	public String generarTokenPrincipal(Authentication authentication) {
		UserDTO userDetails = (UserDTO) authentication.getPrincipal();
		Map<String, Object> claims = new HashMap<>();
		return crearToken(claims, userDetails.getUsername());
	}
	
	private String crearToken(Map<String, Object> claims, String subject) {			
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))				
				.setExpiration(new Date(System.currentTimeMillis() + calcularHorasRestantes() * 1000))
				//.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))	
				//.setExpiration(new Date())
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	public Boolean validarToken(String token, UserDetails userDetails) {
		final String username = getUserNameFromToken(token);
		boolean validaUsername = username.equals(userDetails.getUsername());
		Boolean tokenExpirado = isTokenExpired(token);
		return (validaUsername && !tokenExpirado);
	}
	
	private long calcularHorasRestantes() {
		LocalDateTime hoy = LocalDateTime.now();
		LocalDateTime tomorrow = LocalDateTime.of(hoy.getYear(), hoy.getMonth(), hoy.getDayOfMonth(), 0, 0).plusDays(1);		
		return Duration.between(hoy, tomorrow).getSeconds();
	}

}
