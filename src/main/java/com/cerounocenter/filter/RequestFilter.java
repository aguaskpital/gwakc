package com.cerounocenter.filter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.logging.Level;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.util.StreamUtils;

import com.cerounocenter.entity.DAOLog;
import com.cerounocenter.service.LogService;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class RequestFilter extends ZuulFilter {	
	
	@Autowired
	@Qualifier("logService")
	private LogService logService;

	@Override
	public boolean shouldFilter() {	
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest req = ctx.getRequest();
		System.err.println("ERROR: "+req.getPathInfo());
		ServletInputStream dato = null;
		String cadena = "<----->";
		try {
			dato = ctx.getRequest().getInputStream();
		} catch (IOException e) {
			cadena = e.getMessage();
		}		
		try {
			cadena = StreamUtils.copyToString(dato, Charset.forName("UTF-8"));
		} catch (IOException e) {
			cadena = e.getMessage();
		}
		registraLog(Level.INFO, cadena , ctx.getRequest().getRequestURI(), "run", RequestFilter.class.getCanonicalName(), ctx.getRequest().getRemoteAddr());
		return null;
	}

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return FilterConstants.SEND_RESPONSE_FILTER_ORDER;
	}
	
	protected void registraLog(Level tipoLog, String mensaje, String detalle, String metodo, String className, String ip) {
		Date fechaLog = new Date();
		DAOLog log = new DAOLog();
		log.setDetalleOrigen(detalle);
		log.setFechaLog(fechaLog);
		log.setHoraLog(fechaLog);
		log.setMensaje(mensaje);
		log.setOrigen(className);
		log.setTipoLog(tipoLog.getName());
		log.setMetodo(metodo);
		log.setIp(ip);
		logService.procesa(log);
	}

}
