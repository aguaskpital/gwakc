package com.cerounocenter.filter;

import com.cerounocenter.entity.DAOLog;
import com.cerounocenter.service.LogService;
import com.google.common.io.CharStreams;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.POST_TYPE;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.logging.Level;

import javax.servlet.ServletInputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

@Component
public class ResponseFilter extends ZuulFilter {

	@Autowired
	@Qualifier("logService")
	private LogService logService;

	@Override
	public boolean shouldFilter() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
		InputStream dato = ctx.getResponseDataStream();
		String cadena1 = ctx.getResponseBody();
		//String cadena = StreamUtils.copyToString(dato, Charset.forName("UTF-8"));
		System.out.println("Muestra cadena run ResponseFilter >> "+cadena1);
//		try (final InputStream responseDataStream = context.getResponseDataStream()) {
//
//			if (responseDataStream == null) {
//				registraLog(Level.INFO, "BODY: {}", context.getRequest().getRequestURI(), "run",
//						ResponseFilter.class.getCanonicalName(), context.getRequest().getRemoteAddr());
//				return null;
//			}
//
//			String responseData = CharStreams.toString(new InputStreamReader(responseDataStream, "UTF-8"));
//			registraLog(Level.INFO, responseData, context.getRequest().getRequestURI(), "run",
//					ResponseFilter.class.getCanonicalName(), context.getRequest().getRemoteAddr());
//			context.setResponseBody(responseData);
//			return null;
//		} catch (Exception e) {
//			registraLog(Level.SEVERE, e.getMessage(), context.getRequest().getRequestURI(), "run",
//					ResponseFilter.class.getCanonicalName(), context.getRequest().getRemoteAddr());
//			throw new ZuulException(e, INTERNAL_SERVER_ERROR.value(), e.getMessage());
//		}
		return null;
	}

	@Override
	public String filterType() {
		return POST_TYPE;
	}

	@Override
	public int filterOrder() {
		return 0;
	}

	protected void registraLog(Level tipoLog, String mensaje, String detalle, String metodo, String className,
			String ip) {
		Date fechaLog = new Date();
		DAOLog log = new DAOLog();
		log.setDetalleOrigen(detalle);
		log.setFechaLog(fechaLog);
		log.setHoraLog(fechaLog);
		log.setMensaje(mensaje);
		log.setOrigen(className);
		log.setTipoLog(tipoLog.getName());
		log.setMetodo(metodo);
		log.setIp(ip);
		logService.procesa(log);
	}

}
