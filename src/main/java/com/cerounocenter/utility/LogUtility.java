package com.cerounocenter.utility;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.cerounocenter.entity.DAOLog;
import com.cerounocenter.service.LogService;

public class LogUtility implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	@Qualifier("logService")
	private LogService logService;
	
	protected void registraLog(Level tipoLog, String mensaje, String detalle, String metodo, String className, String ip) {
		Date fechaLog = new Date();
		DAOLog log = new DAOLog();
		log.setDetalleOrigen(detalle);
		log.setFechaLog(fechaLog);
		log.setHoraLog(fechaLog);
		log.setMensaje(mensaje);
		log.setOrigen(className);
		log.setTipoLog(tipoLog.getName());
		log.setMetodo(metodo);
		log.setIp(ip);
		logService.procesa(log);
	}

}
