package com.cerounocenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.cerounocenter.filter.RequestFilter;

@SpringBootApplication
@EnableZuulProxy
public class GwAkcApplication {

	public static void main(String[] args) {
		SpringApplication.run(GwAkcApplication.class, args);
	}

	@Bean
	public RequestFilter preFilter() {
		return new RequestFilter();
	}

}
