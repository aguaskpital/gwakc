package com.cerounocenter.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="log")
public class DAOLog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private Long idlog;

	@Column(name="detalle_origen", nullable=false, length=250)
	private String detalleOrigen;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_log", nullable=false)
	private Date fechaLog;

	@Temporal(TemporalType.TIME)
	@Column(name="hora_log", nullable=false)
	private Date horaLog;

	@Column(length=5000)
	private String mensaje;

	@Column(nullable=false, length=100)
	private String origen;

	@Column(name="tipo_log", nullable=false, length=50)
	private String tipoLog;
	
	@Column(name="metodo", nullable=false, length=50)
	private String metodo;
	
	@Column(name="ip", nullable=false, length=20)
	private String ip;

	public Long getIdlog() {
		return idlog;
	}

	public void setIdlog(Long idlog) {
		this.idlog = idlog;
	}

	public String getDetalleOrigen() {
		return detalleOrigen;
	}

	public void setDetalleOrigen(String detalleOrigen) {
		this.detalleOrigen = detalleOrigen;
	}

	public Date getFechaLog() {
		return fechaLog;
	}

	public void setFechaLog(Date fechaLog) {
		this.fechaLog = fechaLog;
	}

	public Date getHoraLog() {
		return horaLog;
	}

	public void setHoraLog(Date horaLog) {
		this.horaLog = horaLog;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getTipoLog() {
		return tipoLog;
	}

	public void setTipoLog(String tipoLog) {
		this.tipoLog = tipoLog;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}	
	
}
