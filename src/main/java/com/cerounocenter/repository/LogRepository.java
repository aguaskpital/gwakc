package com.cerounocenter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cerounocenter.entity.DAOLog;

@Repository("logRepository")
public interface LogRepository extends CrudRepository<DAOLog, Long> {

}
