package com.cerounocenter.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cerounocenter.entity.Empresa;

@Repository("empresaRepository")
public interface EmpresaRepository extends CrudRepository<Empresa, Long> {

	Boolean existsByEmail(String email);
	Boolean existsByIdentificacion(String email);
	Empresa findByIdentificacion(String identificacion);
	
}
