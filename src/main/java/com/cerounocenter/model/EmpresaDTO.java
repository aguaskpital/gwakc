package com.cerounocenter.model;

import lombok.Data;

@Data
public class EmpresaDTO {

	private Long id;
	private String nombre;
	private String identificacion;
	private String email;
	private String ip;
	private Boolean activo;
	
}
