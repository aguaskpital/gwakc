package com.cerounocenter.model;

import lombok.Data;

@Data
public class UserResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
}
