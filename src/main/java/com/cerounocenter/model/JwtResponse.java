package com.cerounocenter.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -3795261465209764249L;
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
}
