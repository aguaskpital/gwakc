package com.cerounocenter.service.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cerounocenter.entity.DAOLog;
import com.cerounocenter.repository.LogRepository;
import com.cerounocenter.service.LogService;

@Service("logService")
public class LogServiceImpl implements LogService {

	private static Logger logger = Logger.getLogger(LogServiceImpl.class.getName());

	@Autowired
	@Qualifier("logRepository")
	private LogRepository logRepo;

	@Override
	public DAOLog procesa(DAOLog log) {
		try {
			return logRepo.save(log);
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
			return null;
		}
	}

}
