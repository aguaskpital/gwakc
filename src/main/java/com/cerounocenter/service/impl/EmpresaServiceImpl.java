package com.cerounocenter.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.cerounocenter.entity.Empresa;
import com.cerounocenter.model.EmpresaDTO;
import com.cerounocenter.repository.EmpresaRepository;
import com.cerounocenter.service.EmpresaService;

@Service("empresaService")
public class EmpresaServiceImpl implements EmpresaService {

	@Autowired
	@Qualifier("empresaRepository")
	private EmpresaRepository empresaRepository;
	
	@Override
	public String save(EmpresaDTO model) throws Exception {
		Empresa entity = null;
		try {
			ModelMapper mapper = new ModelMapper();
            entity = mapper.map(model, Empresa.class);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		try {
			entity.setActivo(Boolean.TRUE);
			Empresa obj = empresaRepository.save(entity);
			String cadena = String.format("La empresa %d hasido creada existosamente.", obj.getId());
			return cadena;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public String existsEmailEmpresa(String email) throws Exception {
		Boolean exists = empresaRepository.existsByEmail(email);
		if(exists) {
			throw new Exception("El email ya se encuentra registrado.");
		}else {
			return null;
		}
	}

	@Override
	public String existsIdentificacionEmpresa(String identificacion) throws Exception {
		Boolean exists = empresaRepository.existsByIdentificacion(identificacion);
		if(exists) {
			throw new Exception("El número de identificación ya se encuentra registrado.");
		}else {
			return null;
		}
	}

}
