package com.cerounocenter.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cerounocenter.entity.User;
import com.cerounocenter.model.UserDTO;
import com.cerounocenter.model.UserRequest;
import com.cerounocenter.repository.UserRepository;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDTO loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("Usuario %s no encontrado ", username));
		}
		UserDTO dto = new UserDTO();
		dto.setId(user.getId());
		dto.setUsername(user.getUsername());
		dto.setPassword(user.getPassword());		
		return dto;
	}

	public String save(UserRequest model) throws Exception {
		User entity = null;
		try {
			ModelMapper mapper = new ModelMapper();
            entity = mapper.map(model, User.class);
		} catch (Exception e) {
			throw new Exception("Error convert model to entity.");
		}		
		try {
			entity.setPassword(bcryptEncoder.encode(model.getPassword()));
			entity = userRepository.save(entity);
			return String.format("Usuario: %s creado exitosamente, id %d", entity.getUsername(), entity.getId());
		}catch (Exception e) {
			if(e.getMessage().contains("ConstraintViolationException")) {
				throw new Exception("La Empresa que esta asociando al usuario, no existe.");
			}else {				
				throw new Exception(e.getMessage());
			}
		}
	}

	public boolean validarPassword(String passwd, String passwdDB) {
		return bcryptEncoder.matches(passwd, passwdDB);
	}
	
	public boolean existsByUsername(String username) {
		return userRepository.existsByUsername(username);
	}

}
