package com.cerounocenter.service;

import com.cerounocenter.model.EmpresaDTO;

public interface EmpresaService {

	public String save(EmpresaDTO model) throws Exception;
	
	public String existsEmailEmpresa(String email) throws Exception;
	
	public String existsIdentificacionEmpresa(String identificacion) throws Exception;
	
}
