package com.cerounocenter.service;

import com.cerounocenter.entity.DAOLog;

public interface LogService {

	public DAOLog procesa(DAOLog log);
	
}
